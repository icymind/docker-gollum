# docker-gollum

This Dockerfile is a simply install [gollum](https://github.com/gollum/gollum).


## Installation
	docker pull icymind/gollum

## Usage

### Run container simply

    docker run -d -v "$PWD:/wiki" -p 4567:80 --name gollum icymind/gollum

### You can attach some options

	sudo docker run -d -P --name gollum icymind/gollum --allow-uploads --live-preview

To add more options , checkout this link [gollum options](https://github.com/gollum/gollum#running).
